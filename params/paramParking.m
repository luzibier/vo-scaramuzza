%% bootstrappingping 
% selection of candidates
param.bootstrapping.new_candidate.count = 600;

% KLT tracker (vision.PointTracker)
param.bootstrapping.KLT.num_pyramid_levels = 4;
param.bootstrapping.KLT.max_bidirectional_error = 6;
param.bootstrapping.KLT.block_size = [31 31];
param.bootstrapping.KLT.max_iterations = 20;
param.bootstrapping.KLT.min_matches = 10;

%% processing frame
% KLT tracker (vision.PointTracker)
param.processFrame.KLT.num_pyramid_levels = param.bootstrapping.KLT.num_pyramid_levels;
param.processFrame.KLT.max_bidirectional_error = param.bootstrapping.KLT.max_bidirectional_error;
param.processFrame.KLT.block_size = param.bootstrapping.KLT.block_size;
param.processFrame.KLT.max_iterations = param.bootstrapping.KLT.max_iterations;
param.processFrame.KLT.min_matches = param.bootstrapping.KLT.min_matches;

% estimateWorldCameraPose
param.processFrame.eWCP.max_reprojection_error = 1;
param.processFrame.eWCP.max_num_trials = 1000;
param.processFrame.eWCP.confidence = 99.5;
param.processFrame.eWCP.max_movement = 3;

% For adding new landmarks
param.processFrame.optimization.max_iter = 20;
param.processFrame.optimization.max_repr_err = 1;


% reset with a spacing of two frame (one otherwise)
param.reset.twoframes = false;
% selecting new landmarks
param.processFrame.landmarks.alpha_min = deg2rad(0.4);
param.processFrame.landmarks.alpha_max = deg2rad(10.0);

% selecting new candidates
param.processFrame.new_candidate.dist = 8;
param.processFrame.new_candidate.count = 400;