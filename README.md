# VAMR 2020, Mini-project

by Kira Erb, Antonia Mosberger, Schuler Madeleine, Laura Tissari, Luzian Bieri

## Setup

1. Clone the repo / download the zip
2. Copy the datasets into `data` and unzip there. possibly the paths in `main.m` need to be adjusted.

## Running the VO-pipeline

1. Select the requested dataset (make sure that the path is correct) by adjusting the variable `ds` in `main.m`.
2. Run the file `main.m`

## Used Toolboxes

* **Optimization Toolbox**: used to optimize camera pose to reduce reprojection error
* **Image Processing Toolbox** and **Computer Vision Toolbox**: triangulation, pose estimation, point tracking, etc.
* **UAV Toolbox**: used for euler angle calculus

## Recordings

A playlist of screencast of the pipeline in action can be found [here](https://youtube.com/playlist?list=PL1Zmzz8Vqb2pSetZ92C9qGbCKHh07tG38).

The recording has been conducted with `Matlab R2020b` using the following hardware:
* AMD Ryzen 7 1800X Eight-Core Processor (only on one thread) at 3.4 GHz
* 32 GB RAM

## Additional Datasets

Our own recorded datasets can be found [here](https://polybox.ethz.ch/index.php/s/oU1TsqTJvin7B8h).
