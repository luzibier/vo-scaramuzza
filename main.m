%% Setup
clc
clear all

ds = 2; 
% 0: KITTI, 
% 1: Malaga,
% 2: parking
% 3: lasthoenggcurve
% 4: firsthoenggstreet
% 5: middlehoenggstrasse
% 6: lasthoenggcurve_moreframes
% 7: maah

% PATHS 
parking_path = './data/parking';
kitti_path = './data/kitti';
malaga_path = './data/malaga-urban-dataset-extract-07';
lhk_path = './data/lasthoenggcurve';
fhs_path = './data/firsthoenggstreet';
mhs_path = './data/middlehoenggstreet';
lhcmf_path = './data/lasthoenggcurve_moreframes';
maah_path = './data/maah_manyframes';

% which frames are used to start 
bootstrap_frames = [1,3];
addpath(genpath(cd))

if ds == 0
    % need to set kitti_path to folder containing "00" and "poses"
    assert(exist('kitti_path', 'var') ~= 0);
    ground_truth = load([kitti_path '/poses/01.txt']);
    ground_truth = ground_truth(:, [end-8 end]);
    last_frame = 4540;
    K = [7.188560000000e+02 0 6.071928000000e+02
        0 7.188560000000e+02 1.852157000000e+02
        0 0 1];
    paramKitti;
elseif ds == 1
    % Path containing the many files of Malaga 7.
    assert(exist('malaga_path', 'var') ~= 0);
    images = dir([malaga_path ...
        '/malaga-urban-dataset-extract-07_rectified_800x600_Images']);
    left_images = images(3:2:end);
    last_frame = length(left_images);
    K = [621.18428 0 404.0076
        0 621.18428 309.05989
        0 0 1];
    paramMalaga;
elseif ds == 2
    assert(exist('parking_path', 'var') ~= 0);
    last_frame = 598;
    K = load([parking_path '/K.txt']);
     
    ground_truth = load([parking_path '/poses.txt']);
    ground_truth = ground_truth(:, [end-8 end]);
    paramParking;
elseif ds == 3
    assert(exist('lhk_path', 'var') ~= 0);
    last_frame = 102;
    K = load([lhk_path '/K.txt'])';
    paramMalaga;
elseif ds == 4
    assert(exist('fhs_path', 'var') ~= 0);
    last_frame = 129;
    K = load([lhk_path '/K.txt'])';
    paramFhs;
elseif ds == 5
    assert(exist('mhs_path', 'var') ~= 0);
    last_frame = 229;
    K = load([lhk_path '/K.txt'])';
    bootstrap_frames = [15,17];
    paramMhs;   
elseif ds == 6
    assert(exist('lhcmf_path', 'var') ~= 0);
    last_frame = 369;
    K = load([lhk_path '/K.txt'])';   
    bootstrap_frames = [60, 62];
    paramlhcmf;  
elseif ds == 7
    assert(exist('maah_path', 'var') ~= 0);
    last_frame = 254;
    K = load([lhk_path '/K.txt'])';
    paramMaah;      
else
    assert(false);
end

%% Bootstrap
if ds == 0
    img0 = imread([kitti_path '/00/image_0/' ...
        sprintf('%06d.png',bootstrap_frames(1))]);
    img1 = imread([kitti_path '/00/image_0/' ...
        sprintf('%06d.png',bootstrap_frames(2))]);
elseif ds == 1
    img0 = rgb2gray(imread([malaga_path ...
        '/malaga-urban-dataset-extract-07_rectified_800x600_Images/' ...
        left_images(bootstrap_frames(1)).name]));
    img1 = rgb2gray(imread([malaga_path ...
        '/malaga-urban-dataset-extract-07_rectified_800x600_Images/' ...
        left_images(bootstrap_frames(2)).name]));
elseif ds == 2
    img0 = rgb2gray(imread([parking_path ...
        sprintf('/images/img_%05d.png',bootstrap_frames(1))]));
    img1 = rgb2gray(imread([parking_path ...
        sprintf('/images/img_%05d.png',bootstrap_frames(2))]));
elseif ds == 3
    img0 = rgb2gray(imread([lhk_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(1))]));
    img1 = rgb2gray(imread([lhk_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(2))]));
elseif ds == 4
    img0 = rgb2gray(imread([fhs_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(1))]));
    img1 = rgb2gray(imread([fhs_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(2))]));
elseif ds == 5
    img0 = rgb2gray(imread([mhs_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(1))]));
    img1 = rgb2gray(imread([mhs_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(2))]));
elseif ds == 6
    img0 = rgb2gray(imread([lhcmf_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(1))]));
    img1 = rgb2gray(imread([lhcmf_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(2))]));
elseif ds == 7
    img0 = rgb2gray(imread([maah_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(1))]));
    img1 = rgb2gray(imread([maah_path ...
        sprintf('/img_000 (%d).png',bootstrap_frames(2))]));
else
    assert(false);
end
%% INITIALIZATION_Keypoints
[keypts, landmarksInit, position] = bootstrap(img0,img1,param.bootstrapping,K);

%% Continuous operation
% all images except bootstrapped
range = (bootstrap_frames(2)+1):last_frame;

% prepare initial state
% dehomogenize;
state{1} = keypts(:, 1:2);
state{2} = landmarksInit(:, 1:3);
% no candidates
state{3} = [];
state{4} = [];
state{5} = [];

% variables containing the past frames and position (used for occasional
% reset)
pprev_img = img1;
prev_img = img1;
prev_pose = position;

% history is a struct which contains information of the past (position,
% and landmarks)
history{1}.position = position;
history{1}.landmarks = state{2};

for i = range
    fprintf('\n\nProcessing frame %d\n=====================\n', i);
    % retrieve next image
    if ds == 0
        image = imread([kitti_path '/00/image_0/' sprintf('%06d.png',i)]);
        ground_truth = load([kitti_path '/poses/01.txt']);
    elseif ds == 1
        image = rgb2gray(imread([malaga_path ...
            '/malaga-urban-dataset-extract-07_rectified_800x600_Images/' ...
            left_images(i).name]));
    elseif ds == 2
        image = im2uint8(rgb2gray(imread([parking_path ...
            sprintf('/images/img_%05d.png',i)])));
    elseif ds == 3
        image = im2uint8(rgb2gray(imread([lhk_path ...
            sprintf('/img_000 (%d).png',i)])));
    elseif ds == 4
        image = im2uint8(rgb2gray(imread([fhs_path ...
            sprintf('/img_000 (%d).png',i)])));
    elseif ds == 5
        image = im2uint8(rgb2gray(imread([mhs_path ...
            sprintf('/img_000 (%d).png',i)])));
    elseif ds == 6
        image = im2uint8(rgb2gray(imread([lhcmf_path ...
            sprintf('/img_000 (%d).png',i)])));
    elseif ds == 7
        image = im2uint8(rgb2gray(imread([maah_path ...
            sprintf('/img_000 (%d).png',i)])));
    else
        assert(false);
    end
    
    % selects the image and pose used to reset which are one or two steps
    % old based on settings
    if param.reset.twoframes
        pimg = pprev_img;
        ppose = prev_pose;
    else
        pimg = prev_img;
        ppose = position;
    end
    
    % extract position and new state
    [state, position] = processFrame(state, pimg, image, ppose, K, param);
    % create the plots
    visualize(state, image, history);
    
    % Makes sure that plots refresh.    
    pause(.1);
    
    % update history
    idx = i+1-bootstrap_frames(2);
    history{idx}.position = position;
    history{idx}.landmarks = state{2};
    % update vars containting position and image one resp. two frames old
    pprev_img = prev_img;
    prev_img = image;
    prev_pose = position;
end


