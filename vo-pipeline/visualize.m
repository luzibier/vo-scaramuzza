function visualize(state, image, history)
    %%visualize 4 plots 
    % 
    % INPUT: 
    % - state: state{1}= keypoints at current frame, state{2}= landmarks at current frame
    % - image: current image
    % - history.position: past translations 
    % - history.landmarks: past landmarks
    
    %current keypoints, landmarks and translation
    keypoints = state{1};
    landmarks = state{2};
    candidate_keypoints=state{3};
    
    frames = -min(20, length(history)-1):0;
    
    % get number of landmarks and translations over last 20 frames 
    landmarkCounter = zeros(length(frames), 1);
    positions = zeros(length(frames), 3);
    diff=max(length(history)-21,0);
    for i = min(21, length(history))+frames
        landmarkCounter(i) = length(history{i+diff}.landmarks);
        positions(i,:) = history{i+diff}.position(:,4).';
    end
    
    % plot shows candidates (red) and current keypoints (green) on the
    % current image
    figure(3)
    subplot(2,4,[1,2]);
    imshow(image);
    hold on;
    scatter(keypoints(:,1), keypoints(:,2), 'xg');
    scatter(candidate_keypoints(1, :), candidate_keypoints(2, :), 'xr');
    title('Current Image');
    hold off
    
    % plot trajectory of last 20 frames and landmarks
    subplot(2,4,[3,4,7,8]);
    axis square;
    axis equal;
    plot(positions(:,1), positions(:,3), '-');
    hold on;
    radius = 30;
    plot(landmarks(:,1), landmarks(:,3), 'o');
    xlim([positions(end,1)-radius, positions(end,1)+radius])
    ylim([positions(end,3)-radius, positions(end,3)+radius])
    title('trajectory of last 20 frames and landmarks')
    hold off
    
    % plot number of landmarks in the last 20 frames
    last_twenty_frames = -20:0;
    length_landmarkCounter=length(landmarkCounter);
    if length_landmarkCounter < 21
        landmarkCounter=[zeros(21-length_landmarkCounter,1); landmarkCounter];
    end
    subplot(2,4,5);
    plot(last_twenty_frames, landmarkCounter);
    hold on ;
    ylim([0, inf]);
    title('# tracked landmarks over last 20 frames');
    hold off
    
    %plot trajectory 
    sz=length(history);
    allpositions=zeros(sz,3);
    for i=1:sz
        allpositions(i,:)=history{i}.position(:,4)';   
    end
    subplot(2,4,6);
    max_x= max(max(ceil(abs(allpositions(:,1))))+2, min(2*sz, 15));
    max_y= max(ceil(abs(allpositions(:,3))))+2;
    plot(allpositions(:,1), allpositions(:,3), '-');
    hold on;
    title(['Full trajectory. current frame: ',  num2str(length(history))]);
%     ylim([0, maxy]); % uncomment if fixed min at y = 0
%     xlim([-max_x, max_x]);
    max_dir = max(max_y, max_x);
    ylim([-max_dir, max_dir]);
    xlim([-max_dir, max_dir]);
    hold off
    
end