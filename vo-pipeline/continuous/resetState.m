function [s, ns, pos] = resetState(prev_pose, prev_img, image, K, bootstrap_param)
    % resets the pose and states using same function as bootstrapping
    % INPUT:
    %   - prev_pose: previous known pose of the camera
    %   - prev_img: previous frame (where keypoints were found)
    %   - image: current frame
    %   - K: Intrinsic Matrix of Camera (in the form used in the lecture)
    %   - bootstrap_param: params used for bootstrapping
    % RETURNS:
    %   - s: reset state
    %   - ns: reset new state
    %   - pos: new position
    
    % reload landmarks and keypoints
    [keypts, landmarksInit, rel_pos] = bootstrap(prev_img, image, bootstrap_param, K);
    % as poses are relative, translate landmarks back w.r.t. to origin
    lm_hom = [landmarksInit,ones(size(landmarksInit, 1), 1)]';
    res_lm = (prev_pose * lm_hom)';
    % pose is camera to world, we need world to camera
    pos = prev_pose * [rel_pos(:, 1:3), -rel_pos(:, 4); 0 0 0 1];
    % reset state and new state
    ns{1} = keypts;
    ns{2} = res_lm;
    ns{3} = [];
    ns{4} = [];
    ns{5} = [];
    s{1} = keypts;
    s{2} = res_lm;
    s{3} = [];
    s{4} = [];
    s{5} = [];
end