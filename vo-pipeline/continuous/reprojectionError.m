function error = reprojectionError(x, keypoints, landmarks, K)
    % computes the reprojection error of all keypoints
    % INPUT:
    %   - x: pose of the Camera encoded as follows [euler_angles, pose] as
    %        3x2 matrix
    %   - keypoints: 2D coordinates of the detected keypoints
    %   - landmarks: 3D position of the respective landmarks
    %   - K: Intrinsic Matrix of Camera (in the form used in the lecture)
    % RETURNS :
    %   - error: reprojection error of each landmark

    T = [eul2rotm(x(:,1)'), x(:,2); 0 0 0 1];
    % perform projection of 3D landmarks to camera frame
    M = K*T(1:3,:);
    % project landmarks to camera-ccords
    p_projected_hom = M * [landmarks; ones(1,size(landmarks,2))];
    p_projected = p_projected_hom(1:2,:)./p_projected_hom(3,:);
    % calc reprojection error
    error = keypoints' - p_projected;
end