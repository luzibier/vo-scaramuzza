function [matched_keypoints1, matched_keypoints2] = initializeKP(frame1,frame2,param)
    % selects new keypoints for matching
    % INPUT: 
    %   - frame1: first frame used to extract pose and landmarks
    %   - frame2: second frame used to extract pose and landmarks
    %   - param: tunable parameters for different datasets
    % RETURNS: 
    %   - matched_keypoints1: pose of keypoints in the first frame
    %   - matched_keypoints2: pose of keypoints in the second frame
    
    % extract Eigen Features
    sz = size(frame1);
    candidates = detectMinEigenFeatures(frame1,'ROI', [1,1,sz(2), sz(1)]);
    
    % select strongest points and track them through the two frames
    C_new_kpts = double(selectStrongest(candidates, param.new_candidate.count).Location);
    tracker = vision.PointTracker('NumPyramidLevels', param.KLT.num_pyramid_levels, ...
                                  'MaxBidirectionalError', param.KLT.max_bidirectional_error, ...
                                  'BlockSize', param.KLT.block_size, ...
                                  'MaxIterations', param.KLT.max_iterations);
    initialize(tracker, C_new_kpts, frame1);
    [points,validity] = tracker(frame2);
    release(tracker);
    
    % remove all keypoints which could not be tracked.
    matched_keypoints1 = C_new_kpts(validity, :);
    matched_keypoints2 = points(validity, :);
end
