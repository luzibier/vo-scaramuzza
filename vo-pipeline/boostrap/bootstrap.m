function [inl_kp, landmarks, rel_pose] = bootstrap(frame1,frame2,param,K)
    % initializes the camera with a minimum numbers of trackable keypoints
    % (specified in param)
    % INPUT: 
    %   - frame1: first frame used to extract pose and landmarks
    %   - frames: second frame used to extract pose and landmarks
    %   - K: Intrinsic Matrix of Camera (in the form used in the lecture)
    %   - params: tunable parameters for different datasets
    % RETURNS: 
    %   - inl_kp: pose of keypoints in the second frame
    %   - landmarks: 3D position of landmarks w.r.t. camera pose at first frame 
    %   - rel_pose: relative 3D position of camera pose at frame 2
%                   w.r.t. camera pose at first frame 
    while true
        % get keyppoints, extract poses and landmarks
        [p0, p1] = initializeKP(frame1,frame2,param);
        [~, inl_kp, landmarks, rel_pose] = init_loc(p0,p1,K);
        % do until enough trackable keypoints are retrieved
        if length(inl_kp) > param.KLT.min_matches 
           break 
        end
    end
end